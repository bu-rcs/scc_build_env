# scc_build_env

Utility to set and unset common build environment variables using the SCC
module system.

Specifically, the module searches for libraries and headers in the following
locations:

- libraries: LIBRARY_PATH, LD_LIBRARY_PATH, SCC_*_LIB
- headers: CPATH, C_INCLUDE_PATH, CPLUS_INCLUDE_PATH, SCC_*_INCLUDE

The resulting lists of directories used to set standard compiler flags in the
environment variables:

- LDFLAGS: includes -L<path> and -Wl,-rpath,<path> flags
- CFLAGS, CXXFLAGS, FFLAGS: include -I<path> flags

In addition, the module makes copies of these environment variables for the
user to use explicitly in the case where a build system ignores of overwrites
that standard variables. The copies are:

- SCC_LDFLAGS = LDFLAGS
- SCC_CFLAGS = CFLAGS
- SCC_CXXFLAGS = CXXFLAGS
- SCC_FFLAGS = FFLAGS
